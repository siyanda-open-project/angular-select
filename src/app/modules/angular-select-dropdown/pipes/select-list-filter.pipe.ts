import { Pipe, PipeTransform } from '@angular/core';
import {ListItemModel} from "../model/list-item.model";


@Pipe({
  name: 'selectFilter',
  pure: false
})
export class SelectListFilterPipe implements PipeTransform {
  transform(items: ListItemModel[], filter: ListItemModel): ListItemModel[] {
    if (!items || !filter) {
      return items;
    }
    return items.filter((item: ListItemModel) => this.applyFilter(item, filter));
  }

  applyFilter(item: ListItemModel, filter: ListItemModel): boolean {
    if (typeof item.text === 'string' && typeof filter.text === 'string') {
      return !(filter.text && item.text && item.text.toLowerCase().indexOf(filter.text.toLowerCase()) === -1);
    } else {
      return !(filter.text && item.text && item.text.toString().toLowerCase().indexOf(filter.text.toString().toLowerCase()) === -1);
    }
  }
}
