import {Component, OnInit} from '@angular/core';
import {IDropdownSettings} from "./modules/angular-select-dropdown/model/dropdown-settings.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {
  title = 'angular-selector';

  dropdownList = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    searchPlaceholderText: "Search County",
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  dropdownSettingsTwo: IDropdownSettings = {
    singleSelection: true,
    searchPlaceholderText: "Search County",
    allowSearchFilter: true
  };
  dropdownSettingsThree: IDropdownSettings = {
    singleSelection: true,
    searchPlaceholderText: "Search County"
  };

  selectTestFormGroup: FormGroup;

  ngOnInit() {

    this.selectTestFormGroup = new FormGroup({
      selector: new FormControl(['Pune','Navsari'], Validators.required),
      selectorTwo: new FormControl('', Validators.required),
      selectorThree: new FormControl('', Validators.required)
    })
    this.dropdownList = ['Mumbai','Bangaluru','Pune','Navsari' ,'New Delhi'];
  }
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  onSubmit() {
    if (this.selectTestFormGroup.valid) {
      alert('Okay nice!')
    } else {
      alert('Fill form to test validation')
    }
  }
}
